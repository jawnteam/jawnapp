package com.jawndoe.app.repository;

import com.jawndoe.app.domain.Comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Comment entity.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>, JpaSpecificationExecutor<Comment> {

    @Query("select comment from Comment comment where comment.user.login = ?#{principal.preferredUsername}")
    List<Comment> findByUserIsCurrentUser();

    @Query(value = "select distinct comment from Comment comment left join fetch comment.tags",
        countQuery = "select count(distinct comment) from Comment comment")
    Page<Comment> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct comment from Comment comment left join fetch comment.tags")
    List<Comment> findAllWithEagerRelationships();

    @Query("select comment from Comment comment left join fetch comment.tags where comment.id =:id")
    Optional<Comment> findOneWithEagerRelationships(@Param("id") Long id);
}
