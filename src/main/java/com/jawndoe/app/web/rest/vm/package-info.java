/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jawndoe.app.web.rest.vm;
