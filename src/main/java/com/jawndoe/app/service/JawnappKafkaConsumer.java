package com.jawndoe.app.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JawnappKafkaConsumer {

    private final Logger log = LoggerFactory.getLogger(JawnappKafkaConsumer.class);
    private static final String TOPIC = "topic_jawnapp";

    // @KafkaListener(topics = "topic_jawnapp", groupId = "group_id")
    public void consume(String message) throws IOException {
        log.info("Consumed message in {} : {}", TOPIC, message);
    }
}
